note
	description: "Summary description for {PAT_JSON_EVENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_JSON_EVENT

inherit
	PAT_EVENT

create
	make_from_json,
	make_from_separate

feature {NONE} -- Initialization

	make_from_json (j: READABLE_STRING_8)
		do
			json := j
		end

	make_from_separate (ev: separate PAT_EVENT)
		local
			s: STRING
		do
			create s.make_from_separate (ev.as_json)
			make_from_json (s)
		end

feature -- Access

	json: READABLE_STRING_8

feature -- Conversion

	as_json: READABLE_STRING_8
		do
			Result := json
		end


end
