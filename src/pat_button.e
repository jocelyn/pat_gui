note
	description: "Summary description for {PAT_BUTTON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_BUTTON

create
	make_with_text,
	make_with_text_and_action

feature {NONE} -- Initialization

	make_with_text (a_text: READABLE_STRING_GENERAL)
		do
			set_text (a_text)
		end

	make_with_text_and_action (a_text: READABLE_STRING_GENERAL; a_action: PROCEDURE)
		do
			make_with_text (a_text)
			set_action (a_action)
		end

feature -- Access

	text: STRING_32

	action: detachable PROCEDURE

feature -- Change

	set_text (a_text: READABLE_STRING_GENERAL)
		do
			text := a_text.to_string_32
		end

	set_action (act: detachable PROCEDURE)
		do
			action := act
		end

invariant

end
