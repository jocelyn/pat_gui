note
	description: "Summary description for {PAT_MENU_BOX}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_MENU_BOX

inherit
	EV_CELL
		redefine
			initialize,
			create_interface_objects
		end

feature {NONE} -- Initialization

	create_interface_objects
		do
			Precursor
			create box
		end

	initialize
		local
			b: EV_VERTICAL_BOX
		do
			Precursor
			create b
			b.set_padding_width (3)
			b.set_background_color (create {EV_COLOR}.make_with_8_bit_rgb (0,0,127))
			b.extend (box)
			box.set_padding_width (3)
			put (b)
		end

	box: EV_VERTICAL_BOX

feature -- Change

	add_menu_item (a_title: READABLE_STRING_GENERAL)
		local
			b: EV_BUTTON
		do
			create b.make_with_text (a_title)
			box.extend (b)
		end

	add_button (but: PAT_BUTTON)
		local
			b: EV_BUTTON
		do
			create b.make_with_text (but.text)
			if attached but.action as l_action then
				b.select_actions.extend (agent l_action.call (Void))
			end
			box.extend (b)
		end

end
