class
	PAT_WINDOW

inherit
	EV_TITLED_WINDOW
		redefine
			initialize,
			create_interface_objects
		end

	PAT_OBSERVER
		undefine
			default_create,
			copy
		redefine
			on_event
		end

feature {NONE} -- Initialization

	create_interface_objects
		do
			Precursor
			create grid
			create mapping.make_caseless (10)
			create {EV_HORIZONTAL_BOX} header_box
			create {EV_VERTICAL_BOX} first_side_box
			create {EV_VERTICAL_BOX} second_side_box
			create {EV_VERTICAL_BOX} main_box
		end

	initialize
		local
			m: EV_VERTICAL_BOX
			hb: EV_HORIZONTAL_BOX
			l_menu: PAT_MENU_BOX
		do
			Precursor
			create m
			extend_not_expandable (header_box, m)
			create hb
			m.extend (hb)
			extend_not_expandable (first_side_box, hb)
			hb.extend (main_box)
			extend_not_expandable (second_side_box, hb)
			extend (m)

			extend_not_expandable (create {EV_LABEL}.make_with_text ("{PAT}"), header_box)
			header_box.extend (create {EV_CELL})
			header_box.extend (create {EV_LABEL}.make_with_text ("Process All"))
			header_box.extend (create {EV_CELL})
			header_box.extend (create {EV_LABEL}.make_with_text ("../.."))

			extend_not_expandable (create {EV_LABEL}.make_with_text ("{PAT}"), header_box)

			create l_menu
			l_menu.add_button (new_button ("One"))
			l_menu.add_button (new_button ("Two"))
			l_menu.add_button (create {PAT_BUTTON}.make_with_text_and_action ("Toggle Console", agent on_toggle_console))
			extend_not_expandable (l_menu, first_side_box)

			create l_menu
			l_menu.add_button (create {PAT_BUTTON}.make_with_text_and_action ("Run", agent on_run))
			l_menu.add_button (create {PAT_BUTTON}.make_with_text_and_action ("Stop All", agent on_stop_all))
			l_menu.add_button (create {PAT_BUTTON}.make_with_text_and_action ("QUIT", agent on_quit))
			l_menu.add_menu_item ("Time ...")
			extend_not_expandable (l_menu, second_side_box)

			main_box.extend (grid)
			grid.set_column_count_to (3)
			grid.column (1).set_title ("Id")
			grid.column (2).set_title ("Status")
			grid.column (3).set_title ("Value")
			grid.set_minimum_size (300, 300)
		end

	extend_not_expandable (a_widget: EV_WIDGET; a_container: EV_BOX)
		do
			a_container.extend (a_widget)
			a_container.disable_item_expand (a_widget)
		end

feature -- Widget

	grid: EV_GRID

	mapping: STRING_TABLE [EV_GRID_ROW]

	header_box: EV_BOX

	first_side_box: EV_BOX

	second_side_box: EV_BOX

	main_box: EV_BOX

feature -- Factory

	new_button (a_text: READABLE_STRING_GENERAL): PAT_BUTTON
		do
			create Result.make_with_text (a_text)
			Result.set_action (agent on_button_clicked (Result))
		end

feature -- Control

	internal_controller: detachable like controller

	controller: separate PAT_CONTROLLER
		do
			Result := internal_controller
			if Result = Void then
				create Result.make (10, Current)
				internal_controller := Result
			end
		end

	run (a_id: READABLE_STRING_GENERAL)
		do
			separate
				controller as ctler
			do
				ctler.run (a_id)
			end
			start_event_checker
		end

	on_toggle_console
		do
			if console = Void then
				open_console
			else
				close_console
			end
		end

	open_console
		local
			cons: like console
		do
			cons := console
			if cons = Void then
				create cons.make
				console := cons
				main_box.extend (cons)
			end
		end

	close_console
		do
			if attached console as cons then
				console := Void
				main_box.prune_all (cons)
			end
		end

	console_write (m: READABLE_STRING_GENERAL)
		do
			if attached console as cons then
				cons.put_string (m)
			end
		end

	console: detachable PAT_CONSOLE

	start_event_checker
		local
			t: like event_checker_timer
		do
			t := event_checker_timer
			if t = Void then
				create t
				event_checker_timer := t
				t.actions.extend (agent check_events (t))
				t.set_interval (1000)
			end
		end

	event_checker_timer: detachable EV_TIMEOUT

	check_events (t: EV_TIMEOUT)
		local
			lst: ARRAYED_LIST [separate PAT_EVENT]
			nb: INTEGER
		do
			t.set_interval (0)
			separate
				event_handler as h
			do
				nb := h.count
				if nb > 0 then
					console_write ("New events ("+h.count.out + ").%N")
					create lst.make (h.count)
					across
						h as ic
					loop
						lst.force (ic.item)
					end
					h.wipe_out
				else
--					console_write ("No events.%N")
				end
			end
			if lst /= Void then
				ev_event_queue (lst)
			end
			t.set_interval (100)
		end

	ev_event_queue (lst: LIST [separate PAT_EVENT])
		local
			ev: separate PAT_EVENT
		do
			console_write ("ev_event_queue (" + lst.count.out + ")...%N")
			from
				lst.start
			until
				lst.is_empty
			loop
				ev := lst.first
				lst.remove
				on_event (ev)
			end
		end

	event_handler: separate PAT_EVENT_HANDLER
		do
			Result := internal_event_handler
			if Result = Void then
				separate
					controller as ctler
				do
					Result := ctler.event_handler
				end
				internal_event_handler := Result
			end
		end

	internal_event_handler: detachable separate PAT_EVENT_HANDLER

feature -- Event

	on_event (ev: separate PAT_EVENT)
		local
			s, v: STRING_32
			lab: EV_GRID_LABEL_ITEM
		do
			if attached {separate PAT_TEST_EVENT} ev as l_test_ev then
				create s.make_from_separate (l_test_ev.id)
				if attached mapping.item (s) as r then
					create lab.make_with_text (l_test_ev.index.out)
					r.set_item (3, lab)
				end
				console_write ("Event [" + s.as_string_8 + "] -> " + l_test_ev.index.out + " %N")
--				s.append_character ('#')
--				s.append_integer (l_test_event.index)
--				console_write (s.out + "%N")

			else
				create s.make_from_separate (ev.generator)
				console_write ("Event [" + s.as_string_8 + "]%N")
			end
		end

feature -- Event

	on_run
		do
			launch_new_test
		end

	on_stop_all
		do
			separate controller as ctler do
				ctler.request_stop_all
			end
		end

	launch_new_test
		local
			lab: EV_GRID_LABEL_ITEM
			r: EV_GRID_ROW
			cb: EV_GRID_CHECKABLE_LABEL_ITEM
			l_id: STRING
			n: INTEGER
		do
			if attached grid as g then
				g.set_row_count_to (g.row_count + 1)
				n := g.row_count

				r := g.row (n)
				l_id := "job#"+ n.out

				r.set_data (l_id)

				create lab.make_with_text (l_id)
				r.set_item (1, lab)
				create cb.make_with_text ("stopped")
				r.set_item (2, cb)
				create lab.make_with_text ("waiting for data")
				r.set_item (3, lab)
				mapping.force (r, l_id)
				run (l_id)
			end
		end

	on_quit
		do
			ev_application.destroy
		end

	on_button_clicked (but: PAT_BUTTON)
		local
			utf: UTF_CONVERTER
		do
			console_write ("Button clicked [" + utf.utf_32_string_to_utf_8_string_8 (but.text) + "].%N")
		end

invariant

	grid_set: grid /= Void

end
