note
	description: "Summary description for {PAT_EVENT_HANDLER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_EVENT_HANDLER

inherit
	ITERABLE [separate PAT_EVENT]

create
	make

feature {NONE} -- Initialization

	make
		do
			create items.make (1000)
		end

feature -- Access

	count: INTEGER
		do
			Result := items.count
		end

	new_cursor: ITERATION_CURSOR [separate PAT_EVENT]
		do
			Result := items.new_cursor
		end

feature -- Change

	wipe_out
		do
			items.wipe_out
		end

	record (ev: separate PAT_EVENT)
		do
			items.force (ev)
		end

feature -- Access

	items: ARRAYED_LIST [separate PAT_EVENT]

end
