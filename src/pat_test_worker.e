note
	description: "Summary description for {PAT_TEST_WORKER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_TEST_WORKER

inherit
	PAT_WORKER

	SHARED_EXECUTION_ENVIRONMENT

create
	make_with_id

feature {NONE} -- Initialization

	make_with_id (ctler: separate PAT_CONTROLLER; a_id: separate READABLE_STRING_GENERAL)
		do
			make (ctler)
			create id.make_from_separate (a_id)
		end

feature -- Access

	id: IMMUTABLE_STRING_32

feature -- Execution

	execute
		local
			i,n: INTEGER
			ev: PAT_TEST_EVENT
		do
			from
				i := 1
				n := 100
			until
				i > n
			loop
				execution_environment.sleep (100_000_000)
--				print (id.out + "#" + i.out + "%N")
				separate
					controller as ctler
				do
					create ev.make (id)
					ev.set_index (i)
					ctler.notify (ev)
					ev := Void
				end
--				print (id.out + "#" + i.out + " -- COMPLETED!%N")
				if i \\ 10 = 0 then
					check_stop_request
				end
				if stop_requested then
					i := n -- Exit loop!
				end
				i := i + 1
			end
		end

end
