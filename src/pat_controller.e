note
	description: "Summary description for {PAT_CONTROLLER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_CONTROLLER

create
	make

feature {NONE} -- Initialization

	make (nb: INTEGER; obs: separate PAT_OBSERVER)
		do
			observer := obs
			create event_handler.make
			create stop_all_request_status.put (False)
		end

feature -- Access

	observer: separate PAT_OBSERVER

	stop_all_request_status: separate CELL [BOOLEAN]

feature -- Execution

	event_handler: separate PAT_EVENT_HANDLER

	request_stop_all
		do
			separate
				stop_all_request_status as s
			do
				s.replace (True)
			end
		end

	reset_stop_all
		do
			separate
				stop_all_request_status as s
			do
				s.replace (False)
			end
		end

	notify (ev: separate PAT_EVENT)
		do
			separate
				event_handler as h
			do
				if attached {separate PAT_TEST_EVENT} ev as tev then
					h.record (create {PAT_TEST_EVENT}.make_from_separate (tev))
				else
					h.record (create {PAT_JSON_EVENT}.make_from_separate (ev))
				end
			end
		end

	run (a_id: separate READABLE_STRING_GENERAL)
		local
			w: separate PAT_TEST_WORKER
		do
			reset_stop_all
			create w.make_with_id (Current, a_id)
			execute_worker (w)
		end

	execute_worker (w: separate PAT_WORKER)
		do
			w.launch
		end

end
