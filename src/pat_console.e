note
	description: "Summary description for {PAT_CONSOLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_CONSOLE

create
	make

convert
	widget: {EV_WIDGET}

feature {NONE} -- Initialization

	make
		do
			create text
		end

feature -- Access

	widget: EV_WIDGET
		do
			Result := text
		end

	text: EV_TEXT

feature -- Output

	put_string (s: READABLE_STRING_GENERAL)
		do
			text.append_text (s)
			text.set_caret_position (text.text_length)
		end

invariant

end
