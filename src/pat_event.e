note
	description: "Summary description for {PAT_EVENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PAT_EVENT

feature -- Conversion

	as_json: READABLE_STRING_8
		deferred
		end

end
