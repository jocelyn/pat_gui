note
	description: "Summary description for {PAT_TEST_EVENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAT_TEST_EVENT

inherit
	PAT_EVENT

create
	make,
	make_from_separate

feature {NONE} -- Initialization	

	make (a_id: READABLE_STRING_GENERAL)
		do
			create id.make_from_string_general (a_id)
		end

	make_from_separate (ev: separate PAT_TEST_EVENT)
		do
			create id.make_from_separate (ev.id)
			index := ev.index
		end

feature -- Access		

	id: IMMUTABLE_STRING_32

	index: INTEGER

feature -- Status report

	debug_output: STRING_32
		do
			Result := id + "#" + index.out
		end

feature -- Change

	set_index (idx: INTEGER)
		do
			index := idx
		end

feature -- Conversion

	as_json: STRING_8
		do
			create Result.make (10)
			Result.append_character ('{')
				-- type
			Result.append_character ('"')
			Result.append ("type")
			Result.append_character ('"')
			Result.append_character ('=')
			Result.append_character ('"')
			Result.append (generator)
			Result.append_character ('"')
			Result.append_character (',')

				-- id
			Result.append_character ('"')
			Result.append ("id")
			Result.append_character ('"')
			Result.append_character ('=')
			Result.append_character ('"')
			Result.append (id.out)
			Result.append_character ('"')
			Result.append_character (',')
				--index
			Result.append_character ('"')
			Result.append ("index")
			Result.append_character ('"')
			Result.append_character ('=')
			Result.append (index.out)
			Result.append_character ('}')
		end

end
