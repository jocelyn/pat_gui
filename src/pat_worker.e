note
	description: "Summary description for {PAT_WORKER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PAT_WORKER

feature -- Initialization

	make (ctler: separate PAT_CONTROLLER)
		do
			controller := ctler
			stop_all_request_status := ctler.stop_all_request_status
		end

feature -- Access

	controller: separate PAT_CONTROLLER

	stop_all_request_status: separate CELL [BOOLEAN]

	stop_requested: BOOLEAN

	is_running: BOOLEAN

	is_completed: BOOLEAN

feature -- Execution

	launch
		do
			on_start
			execute
			on_completed
		end

feature {NONE} -- Execution

	execute
		deferred
		end

	check_stop_request
		do
			separate
				stop_all_request_status as s
			do
				stop_requested := s.item
			end
		end

	on_start
		do
			stop_requested := False
			is_completed := False
			is_running := True
		end

	on_completed
		do
			is_completed := True
			is_running := False
		end

end
